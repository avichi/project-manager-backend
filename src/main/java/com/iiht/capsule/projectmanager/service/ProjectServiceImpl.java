package com.iiht.capsule.projectmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.repo.ProjectRepository;

@Service
public class ProjectServiceImpl implements ProjectService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectServiceImpl.class);
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	UserService userService;

	@Override
	public Project addProject(Project project) {
		return projectRepository.save(project);
	}

	@Override
	public List<Project> getAllProjects() {
		List<Project> Projects = new ArrayList<>();
		projectRepository.findAll().forEach(Projects::add);
		return Projects;
	}

	@Override
	public Project endUpdateProject(Project project, Long Id) {
		Project ProjectAvailable = projectRepository.findById(Id).orElse(null);
		LOGGER.info("Project Available" + ProjectAvailable.toString());
		if (ProjectAvailable != null) {
			ProjectAvailable.setIsCompleted(project.getIsCompleted());
			LOGGER.info("Project Before Update" + ProjectAvailable.toString());
			projectRepository.save(project);
		}
		return ProjectAvailable;

	}

	@Override
	public Project updateProject(Project project, Long Id) {
		Project ProjectAvailable = projectRepository.findById(Id).orElse(null);
		User userAvailable = userService.getUser(project.getManager().getUserId());
		project.setManager(userAvailable);
		LOGGER.info("Project Available" + ProjectAvailable.toString());
		if (ProjectAvailable != null && userAvailable != null) {
			project.setProjectId(Id);
			projectRepository.save(project);
		}
		return ProjectAvailable;

	}

	@Override
	public Project getProject(Long Id) {
		Project ProjectAvailable = projectRepository.findById(Id).orElse(null);
		return ProjectAvailable;
	}

	@Override
	public void deleteProject(Long Id) {

		projectRepository.deleteById(Id);

	}

}
