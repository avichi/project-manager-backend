package com.iiht.capsule.projectmanager.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.model.Task;
import com.iiht.capsule.projectmanager.model.TestDataService;
import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.repo.TaskRepository;

public class TaskServiceTest {

	@InjectMocks
	TaskServiceImpl taskService;

	@Mock
	private ProjectService projectService;

	@Mock
	private UserService userService;

	@Mock
	private ParentTaskService parentTaskService;

	@Mock
	TaskRepository taskRepo;

	private Project project1 = new Project();

	private Project project2 = new Project();

	private User manager = new User();

	private User user2 = new User();

	private ParentTask pt = new ParentTask();

	private Task task1 = new Task();

	private Task task2 = new Task();

	private TestDataService testDataService = new TestDataService();

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		testDataService.getTestData(manager, user2, project1, project2, pt, task1, task2);
	}

	@Test
	public void getAllTasksTest() throws ParseException {

		List<Task> task = new ArrayList<Task>();
		task.add(task1);
		task.add(task2);
		when(taskRepo.findAll()).thenReturn(task);
		List<Task> taskList = taskService.getAllTasks();
		assertEquals(2, taskList.size());
		verify(taskRepo, times(1)).findAll();

	}

	@Test
	public void createTaskTest() throws ParseException {
		when(projectService.getProject(project1.getProjectId())).thenReturn(project1);
		when(parentTaskService.getParentTask(pt.getId())).thenReturn(pt);
		taskService.addTask(task1);
		verify(taskRepo, times(1)).save(task1);
	}

	@Test
	public void createTaskNotAvilableTest() throws ParseException {
		when(projectService.getProject(project1.getProjectId())).thenReturn(project1);
		when(parentTaskService.getParentTask(pt.getId())).thenReturn(null);
		taskService.addTask(task1);
		verify(taskRepo, times(1)).save(task1);
	}

	@Test
	public void getTaskTest() {
		when(taskRepo.findById(1L)).thenReturn(Optional.of(task1));
		Task task = taskService.getTask(1L);
		assertEquals(1, task.getTaskId());
		assertEquals(false, task.getIsCompleted());
		assertEquals("Task_1", task.getTask());
	}

	@Test
	public void deleteTaskTest() {
		taskService.deleteTask(task1.getTaskId());
		verify(taskRepo, times(1)).deleteById(task1.getTaskId());
		verifyNoMoreInteractions(taskRepo);
	}

	@Test
	public void updateTaskTest() {

		when(taskRepo.findById(task2.getTaskId())).thenReturn(Optional.of(task1));
		when(userService.getUser(manager.getUserId())).thenReturn(manager);
		when(projectService.getProject(project1.getProjectId())).thenReturn(project1);
		taskService.updateTask(task2, task2.getTaskId());
		ArgumentCaptor<Task> taskArgument = ArgumentCaptor.forClass(Task.class);
		verify(taskRepo, times(1)).save(taskArgument.capture());
		Task taskValue = taskArgument.getValue();
		assertNotNull(taskValue.getTaskId());
		assertThat(taskValue.getTask(), is(task2.getTask()));
		assertThat(taskValue.getIsCompleted(), is(task2.getIsCompleted()));
	}

	@Test
	public void endUpdateProjectTest() {
		when(projectService.getProject(project1.getProjectId())).thenReturn(project1);
		when(taskRepo.findById(task2.getTaskId())).thenReturn(Optional.of(task1));
		taskService.endUpdateTask(task2, task2.getTaskId());
		ArgumentCaptor<Task> taskArgument = ArgumentCaptor.forClass(Task.class);
		verify(taskRepo, times(1)).save(taskArgument.capture());
		Task taskValue = taskArgument.getValue();
		assertNotNull(taskValue.getTaskId());
		assertThat(taskValue.getTask(), is(task1.getTask()));
		assertThat(taskValue.getIsCompleted(), is(task1.getIsCompleted()));
	}

}
