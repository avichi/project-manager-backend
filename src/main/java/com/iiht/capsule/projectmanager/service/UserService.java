package com.iiht.capsule.projectmanager.service;

import java.util.List;

import com.iiht.capsule.projectmanager.model.User;

public interface UserService {

	User addUser(User user);

	List<User> getAllUsers();

	User getUser(Long Id);

	User updateUser(User user, Long Id);

	void deleteUser(Long Id);
}
