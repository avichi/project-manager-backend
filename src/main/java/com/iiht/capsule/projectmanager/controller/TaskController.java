package com.iiht.capsule.projectmanager.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iiht.capsule.projectmanager.model.Task;
import com.iiht.capsule.projectmanager.service.TaskService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class TaskController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

	@Autowired
	private TaskService taskService;

	@RequestMapping("/tasks")
	public List<Task> getAllTasks() {
		return taskService.getAllTasks();
	}

	@RequestMapping("/tasks/{id}")
	public ResponseEntity<Object> getTask(@PathVariable long id) {
		LOGGER.info("**********ID***********" + id);
		Task updatedTask = taskService.getTask(id);

		if (updatedTask != null) {
			return ResponseEntity.ok(updatedTask);
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Task not found with ID " + id);

	}

	@PostMapping("/tasks")
	public ResponseEntity<Object> createTask(@RequestBody Task task) {
		LOGGER.info("********************" + task.toString());
		taskService.addTask(task);
		return new ResponseEntity<Object>(task, HttpStatus.CREATED);

	}

	@PutMapping("/endtask/{id}")
	public ResponseEntity<Object> endTask(@RequestBody Task task, @PathVariable Long id) {
		LOGGER.info("*********Task***********" + task.toString());
		LOGGER.info("**********ID***********" + id);
		Task Task = taskService.getTask(id);
		if (Task != null) {
			taskService.endUpdateTask(task, id);
			return new ResponseEntity<Object>(task, HttpStatus.OK);
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Task not found with ID " + id);

	}

	@PutMapping("/tasks/{id}")
	public ResponseEntity<Object> updateTask(@RequestBody Task task, @PathVariable Long id) {
		LOGGER.info("*********Task***********" + task.toString());
		LOGGER.info("**********ID***********" + id);
		Task Task = taskService.getTask(id);
		if (Task != null) {
			taskService.updateTask(task, id);
			return new ResponseEntity<Object>(task, HttpStatus.OK);
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Task not found with ID " + id);

	}

	@DeleteMapping("/tasks/{id}")
	public ResponseEntity<Object> deleteTask(@PathVariable Long id) {
		LOGGER.info("**********ID***********" + id);
		Task task = taskService.getTask(id);

		if (task != null) {
			taskService.deleteTask(id);

			return ResponseEntity.status(HttpStatus.OK).body("");
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Task not found with ID " + id);

	}

}
