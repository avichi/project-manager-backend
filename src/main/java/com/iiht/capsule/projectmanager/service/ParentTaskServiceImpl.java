package com.iiht.capsule.projectmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.repo.ParentTaskRepository;

@Service
public class ParentTaskServiceImpl implements ParentTaskService {

	@Autowired
	private ParentTaskRepository parentTaskRepository;

	@Override
	public ParentTask addParentTask(ParentTask parentTask) {
		return parentTaskRepository.save(parentTask);
	}

	@Override
	public List<ParentTask> getAllParentTasks() {
		List<ParentTask> ParentTasks = new ArrayList<>();
		parentTaskRepository.findAll().forEach(ParentTasks::add);
		return ParentTasks;
	}

	@Override
	public ParentTask updateParentTask(ParentTask parentTask, Long Id) {
		ParentTask ParentTaskAvailable = parentTaskRepository.findById(Id).orElse(null);
		if (ParentTaskAvailable != null) {
			parentTask.setId(Id);
			parentTaskRepository.save(parentTask);
		}
		return ParentTaskAvailable;
	}

	@Override
	public ParentTask getParentTask(Long Id) {
		ParentTask ParentTaskAvailable = parentTaskRepository.findById(Id).orElse(null);
		return ParentTaskAvailable;
	}

	@Override
	public void deleteParentTask(Long Id) {

		parentTaskRepository.deleteById(Id);

	}

	@Override
	public ParentTask getParentTaskByName(String parentTaskName) {
		ParentTask ParentTaskAvailable = parentTaskRepository.findByParentTaskName(parentTaskName);
		return ParentTaskAvailable;
	}

}
