package com.iiht.capsule.projectmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.repo.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public User addUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> getAllUsers() {
		List<User> Users = new ArrayList<>();
		userRepository.findAll().forEach(Users::add);
		return Users;
	}

	@Override
	public User updateUser(User user, Long Id) {
		User userAvailable = userRepository.findById(Id).orElse(null);
		LOGGER.info("UserAvailable: " + userAvailable);
		if (userAvailable != null) {
			user.setUserId(Id);
			LOGGER.info("SaveUser" + user);
			userRepository.save(user);
		}
		return userAvailable;
	}

	@Override
	public User getUser(Long Id) {
		User UserAvailable = userRepository.findById(Id).orElse(null);
		return UserAvailable;
	}

	@Override
	public void deleteUser(Long Id) {

		userRepository.deleteById(Id);

	}

}
