package com.iiht.capsule.projectmanager.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.model.Task;
import com.iiht.capsule.projectmanager.model.TestDataService;
import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.repo.ProjectRepository;

public class ProjectServiceTest {

	@InjectMocks
	ProjectServiceImpl projectService;

	@Mock
	private UserService userService;

	@Mock
	ProjectRepository projectRepo;

	private Project project1 = new Project();

	private Project project2 = new Project();

	private User manager = new User();

	private User user2 = new User();

	private ParentTask pt = new ParentTask();

	private Task task1 = new Task();

	private Task task2 = new Task();

	private TestDataService testDataService = new TestDataService();

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		testDataService.getTestData(manager, user2, project1, project2, pt, task1, task2);
	}

	@Test
	public void getAllProjectTest() throws ParseException {

		List<Project> project = new ArrayList<Project>();
		project.add(project1);
		project.add(project2);
		when(projectRepo.findAll()).thenReturn(project);

		List<Project> projectList = projectService.getAllProjects();
		assertEquals(2, projectList.size());
		verify(projectRepo, times(1)).findAll();

	}

	@Test
	public void createProjectTest() throws ParseException {

		projectService.addProject(project1);
		verify(projectRepo, times(1)).save(project1);
	}

	@Test
	public void getProjectTest() {
		when(projectRepo.findById(1L)).thenReturn(Optional.of(project1));

		Project project = projectService.getProject(1L);

		assertEquals("Project_1", project.getProjectName());
		assertEquals(1, project.getProjectId());
		assertEquals(10, project.getCompletedTasks());
	}

	@Test
	public void deleteProjectTest() {

		projectService.deleteProject(project1.getProjectId());

		verify(projectRepo, times(1)).deleteById(project1.getProjectId());
		verifyNoMoreInteractions(projectRepo);
	}

	@Test
	public void updateProjectTest() {

		when(userService.getUser(manager.getUserId())).thenReturn(manager);
		when(projectRepo.findById(project2.getProjectId())).thenReturn(Optional.of(project1));
		projectService.updateProject(project2, project2.getProjectId());
		ArgumentCaptor<Project> projectArgument = ArgumentCaptor.forClass(Project.class);
		verify(projectRepo, times(1)).save(projectArgument.capture());
		Project projectValue = projectArgument.getValue();
		assertNotNull(projectValue.getProjectId());
		assertThat(projectValue.getProjectName(), is(project2.getProjectName()));
		assertThat(projectValue.getIsCompleted(), is(project2.getIsCompleted()));
	}

	@Test
	public void endUpdateProjectTest() {
		when(projectRepo.findById(project2.getProjectId())).thenReturn(Optional.of(project1));
		projectService.endUpdateProject(project2, project2.getProjectId());
		ArgumentCaptor<Project> projectArgument = ArgumentCaptor.forClass(Project.class);
		verify(projectRepo, times(1)).save(projectArgument.capture());
		Project projectValue = projectArgument.getValue();
		assertNotNull(projectValue.getProjectId());
		assertThat(projectValue.getProjectName(), is(project2.getProjectName()));
		assertThat(projectValue.getIsCompleted(), is(project2.getIsCompleted()));
	}

}
