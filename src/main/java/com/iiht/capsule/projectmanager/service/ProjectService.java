package com.iiht.capsule.projectmanager.service;

import java.util.List;

import com.iiht.capsule.projectmanager.model.Project;

public interface ProjectService {

	Project addProject(Project project);

	List<Project> getAllProjects();

	Project getProject(Long Id);

	Project updateProject(Project project, Long Id);

	Project endUpdateProject(Project project, Long Id);

	void deleteProject(Long Id);
}
