package com.iiht.capsule.projectmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.model.Task;
import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.repo.TaskRepository;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	ParentTaskService parentTaskService;

	@Autowired
	ProjectService projectService;

	@Autowired
	UserService userService;

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskServiceImpl.class);

	@Override
	public Task addTask(Task task) {

		String parentTask = task.getParentTask().getParentTaskName();
		Project project = projectService.getProject(task.getProject().getProjectId());
		LOGGER.info("Parent Task Name: " + parentTask);
		ParentTask pt;
		if (!parentTask.isEmpty() && parentTask != null) {
			LOGGER.info("Parent Task Not Null: " + parentTask);
			pt = parentTaskService.getParentTaskByName(parentTask);
			LOGGER.info("Parent Task Found with Name: " + pt);
			if (pt == null) {
				pt = new ParentTask();
				pt.setParentTaskName(parentTask);
				parentTaskService.addParentTask(pt);
				LOGGER.info("Parent Task Returned Null and Value added: " + pt);
			}
		}
		else {
			pt = parentTaskService.getParentTask(1L);
			LOGGER.info("Parent Task Null: " + pt);
		}
		int taskCount = project.getNumberofTasks() + 1;
		project.setNumberofTasks(taskCount);
		projectService.updateProject(project, project.getProjectId());
		task.setParentTask(pt);
		return taskRepository.save(task);
	}

	@Override
	public List<Task> getAllTasks() {
		List<Task> tasks = new ArrayList<>();
		taskRepository.findAll().forEach(tasks::add);
		tasks.forEach((p) -> LOGGER.info("User: " + p.getUser()));
		return tasks;
	}

	@Override
	public Task endUpdateTask(Task task, Long Id) {

		Task taskAvailable = taskRepository.findById(Id).orElse(null);
		LOGGER.info("TaskAvailable: " + taskAvailable);
		LOGGER.info("TaskAvailable before: " + taskAvailable.getIsCompleted());
		LOGGER.info("TaskAvailable - Task Update: " + task.getIsCompleted());
		if (taskAvailable != null) {
			if (task.getIsCompleted()) {
				Project project = projectService.getProject(taskAvailable.getProject().getProjectId());
				LOGGER.info("ProjectAvailable: " + project);
				LOGGER.info("Project Completed tasks before: " + project.getCompletedTasks());
				project.setCompletedTasks(project.getCompletedTasks() + 1);
				LOGGER.info("Project Completed tasks after: " + project.getCompletedTasks());
				projectService.updateProject(project, project.getProjectId());
				taskAvailable.setIsCompleted(task.getIsCompleted());
			}

			LOGGER.info("TaskAvailable after: " + taskAvailable.getIsCompleted());
			LOGGER.info("TaskAvailable before save: " + taskAvailable);
			taskRepository.save(taskAvailable);
		}
		return taskAvailable;
	}

	@Override
	public Task updateTask(Task task, Long Id) {
		Task taskAvailable = taskRepository.findById(Id).orElse(null);
		String parentTask = task.getParentTask().getParentTaskName();
		ParentTask pt = new ParentTask();
		LOGGER.info("TaskAvailable before: " + taskAvailable.getIsCompleted());
		if (taskAvailable != null) {
			User user = userService.getUser(task.getUser().getUserId());
			task.setUser(user);
			LOGGER.info("Available User" + user.toString());
			if (!parentTask.isEmpty() && parentTask != null) {
				LOGGER.info("Parent Task Not Null: " + parentTask);
				if (parentTask.equals("No Parent Task")) {
					pt.setId(1L);
					pt.setParentTaskName(parentTask);
				}
				else {
					pt = parentTaskService.getParentTaskByName(parentTask);
					LOGGER.info("Parent Task Found with Name: " + pt);
				}
				if (pt == null) {
					pt = new ParentTask();
					pt.setParentTaskName(parentTask);
					parentTaskService.addParentTask(pt);
					LOGGER.info("Parent Task Returned Null and Value added: " + pt);
				}
			}

			else {
				pt = parentTaskService.getParentTask(1L);
				LOGGER.info("Parent Task Null: " + pt);
			}
			task.setParentTask(pt);
			task.setIsCompleted(taskAvailable.getIsCompleted());
			Project previousProject = projectService.getProject(taskAvailable.getProject().getProjectId());
			int taskCount = previousProject.getNumberofTasks() - 1;
			previousProject.setNumberofTasks(taskCount);
			projectService.updateProject(previousProject, previousProject.getProjectId());
			LOGGER.info("Previous Project" + previousProject.toString());
			Project currentProject = projectService.getProject(task.getProject().getProjectId());
			taskCount = currentProject.getNumberofTasks() + 1;
			currentProject.setNumberofTasks(taskCount);
			projectService.updateProject(currentProject, currentProject.getProjectId());
			LOGGER.info("Current Project" + currentProject.toString());
			task.setProject(currentProject);
			task.setTaskId(Id);
			LOGGER.info("*****************************Task Before Save*****************************" + task.toString());
			taskRepository.save(task);
		}

		return taskAvailable;
	}

	@Override
	public Task getTask(Long Id) {
		Task taskAvailable = taskRepository.findById(Id).orElse(null);
		return taskAvailable;
	}

	@Override
	public void deleteTask(Long Id) {

		taskRepository.deleteById(Id);

	}

}
