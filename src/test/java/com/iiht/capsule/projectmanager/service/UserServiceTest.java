package com.iiht.capsule.projectmanager.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.model.Task;
import com.iiht.capsule.projectmanager.model.TestDataService;
import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.repo.UserRepository;

public class UserServiceTest {

	@InjectMocks
	UserServiceImpl userService;

	@Mock
	UserRepository userRepo;

	private Project project1 = new Project();

	private Project project2 = new Project();

	private User user1 = new User();

	private User user2 = new User();

	private ParentTask pt = new ParentTask();

	private Task task1 = new Task();

	private Task task2 = new Task();

	private TestDataService testDataService = new TestDataService();

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		testDataService.getTestData(user1, user2, project1, project2, pt, task1, task2);
	}

	@Test
	public void getAllUsersTest() throws ParseException {

		List<User> user = new ArrayList<User>();
		user.add(user1);
		user.add(user2);
		when(userRepo.findAll()).thenReturn(user);
		List<User> userList = userService.getAllUsers();
		assertEquals(2, userList.size());
		verify(userRepo, times(1)).findAll();

	}

	@Test
	public void createUserTest() throws ParseException {
		userService.addUser(user1);
		verify(userRepo, times(1)).save(user1);
	}

	@Test
	public void getUserTest() {
		when(userRepo.findById(1L)).thenReturn(Optional.of(user1));
		User user = userService.getUser(1L);
		assertEquals("FN", user.getFirstName());
		assertEquals("LN", user.getLastName());
		assertEquals("1", user.getEmployeeId());
	}

	@Test
	public void deleteUserTest() {
		userService.deleteUser(user1.getUserId());
		verify(userRepo, times(1)).deleteById(user1.getUserId());
		verifyNoMoreInteractions(userRepo);
	}

	@Test
	public void updateUserTest() {

		when(userRepo.findById(user2.getUserId())).thenReturn(Optional.of(user1));

		userService.updateUser(user2, user2.getUserId());
		ArgumentCaptor<User> userArgument = ArgumentCaptor.forClass(User.class);
		verify(userRepo, times(1)).save(userArgument.capture());
		User userValue = userArgument.getValue();
		assertNotNull(userValue.getUserId());
		assertThat(userValue.getFirstName(), is(user2.getFirstName()));
		assertThat(userValue.getLastName(), is(user2.getLastName()));
	}

}
