package com.iiht.capsule.projectmanager.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.model.Task;
import com.iiht.capsule.projectmanager.model.TestDataService;
import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.service.ProjectService;
import com.iiht.capsule.projectmanager.service.UserService;
import com.iiht.capsule.projectmanager.util.TestUtil;

@RunWith(SpringRunner.class)
@ContextConfiguration({ "classpath*:spring-test.xml" })
public class ProjectControllerTest {

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Mock
	private ProjectService projectService;

	@Mock
	private UserService userService;

	private MockMvc mockMvc;

	private Project project1 = new Project();

	private Project project2 = new Project();

	private User manager = new User();

	private User user2 = new User();

	private ParentTask pt = new ParentTask();

	private Task task1 = new Task();

	private Task task2 = new Task();

	private TestDataService testDataService = new TestDataService();

	@InjectMocks
	private ProjectController projectController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(projectController).build();
		testDataService.getTestData(manager, user2, project1, project2, pt, task1, task2);
	}

	@Test
	public void getAllProjects() throws IOException, ParseException, Exception {

		List<Project> projects = new ArrayList<Project>();
		projects.add(project1);
		projects.add(project2);

		when(projectService.getAllProjects()).thenReturn(projects);
		mockMvc.perform(get("/projects")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].projectId", is(1))).andExpect(jsonPath("$[0].projectName", is("Project_1")))
				.andExpect(jsonPath("$[0].startDate", is(notNullValue())))
				.andExpect(jsonPath("$[0].endDate", is(notNullValue()))).andExpect(jsonPath("$[0].priority", is(10)))
				.andExpect(jsonPath("$[0].manager.userId", is(1)))
				.andExpect(jsonPath("$[0].manager.firstName", is("FN")))
				.andExpect(jsonPath("$[0].manager.lastName", is("LN")))
				.andExpect(jsonPath("$[0].manager.employeeId", is("1")))
				.andExpect(jsonPath("$[0].numberofTasks", is(20))).andExpect(jsonPath("$[0].completedTasks", is(10)))
				.andExpect(jsonPath("$[0].isCompleted", is(false))).andExpect(jsonPath("$[1].projectId", is(2)))
				.andExpect(jsonPath("$[1].projectName", is("Project_2")))
				.andExpect(jsonPath("$[1].startDate", is(notNullValue())))
				.andExpect(jsonPath("$[1].endDate", is(notNullValue()))).andExpect(jsonPath("$[1].priority", is(20)))
				.andExpect(jsonPath("$[1].manager.userId", is(1)))
				.andExpect(jsonPath("$[1].manager.firstName", is("FN")))
				.andExpect(jsonPath("$[1].manager.lastName", is("LN")))
				.andExpect(jsonPath("$[1].manager.employeeId", is("1")))
				.andExpect(jsonPath("$[1].numberofTasks", is(20))).andExpect(jsonPath("$[1].completedTasks", is(10)))
				.andExpect(jsonPath("$[1].isCompleted", is(true))).andDo(print());
		verify(projectService, times(1)).getAllProjects();
		verifyNoMoreInteractions(projectService);
	}

	@Test
	public void createProject() throws Exception {

		when(projectService.addProject(project1)).thenReturn(project1);

		mockMvc.perform(post("/projects").contentType(APPLICATION_JSON_UTF8).content(TestUtil.ObjecttoJSON(project1)))
				.andExpect(status().isCreated()).andExpect(jsonPath("$.projectId", is(1)))
				.andExpect(jsonPath("$.projectName", is("Project_1")))
				.andExpect(jsonPath("$.startDate", is(notNullValue())))
				.andExpect(jsonPath("$.endDate", is(notNullValue()))).andExpect(jsonPath("$.priority", is(10)))
				.andExpect(jsonPath("$.manager.userId", is(1))).andExpect(jsonPath("$.manager.firstName", is("FN")))
				.andExpect(jsonPath("$.manager.lastName", is("LN")))
				.andExpect(jsonPath("$.manager.employeeId", is("1"))).andExpect(jsonPath("$.numberofTasks", is(20)))
				.andExpect(jsonPath("$.completedTasks", is(10))).andDo(print());
	}

	@Test
	public void getProject() throws Exception {

		when(projectService.getProject(1L)).thenReturn(project1);

		mockMvc.perform(get("/projects/{id}", "1")).andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void getProjectNotAvailable() throws Exception {
		when(projectService.getProject(1L)).thenReturn(null);

		mockMvc.perform(get("/projects/{id}", "1")).andExpect(status().is4xxClientError()).andDo(print());
	}

	@Test
	public void deleteProject() throws Exception {

		when(projectService.getProject(project1.getProjectId())).thenReturn(project1);

		mockMvc.perform(delete("/projects/{id}", project1.getProjectId())).andExpect(status().isOk()).andDo(print());

	}

	@Test
	public void deleteProjectNotAvailable() throws Exception {
		when(projectService.getProject(1L)).thenReturn(null);

		mockMvc.perform(delete("/projects/{id}", "1")).andExpect(status().isNotFound()).andDo(print());

	}

	@Test
	public void updateProject() throws Exception {

		when(projectService.getProject(project2.getProjectId())).thenReturn(project1);
		when(projectService.updateProject(project2, project2.getProjectId())).thenReturn(project1);

		mockMvc.perform(put("/projects/{id}", project2.getProjectId()).contentType(APPLICATION_JSON_UTF8)
				.content(TestUtil.ObjecttoJSON(project1))).andExpect(status().isOk())
				.andExpect(jsonPath("$.projectName", is("Project_1"))).andDo(print());

	}

	@Test
	public void updateProjectNotAvilable() throws Exception {

		when(projectService.getProject(project1.getProjectId())).thenReturn(null);

		mockMvc.perform(put("/projects/{id}", project1.getProjectId()).contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.ObjecttoJSON(project1))).andExpect(status().isNotFound()).andDo(print());

	}

	@Test
	public void updateEndProject() throws Exception {

		when(projectService.getProject(project2.getProjectId())).thenReturn(project1);
		when(projectService.endUpdateProject(project2, project2.getProjectId())).thenReturn(project1);

		mockMvc.perform(put("/endProject/{id}", project2.getProjectId()).contentType(APPLICATION_JSON_UTF8)
				.content(TestUtil.ObjecttoJSON(project1))).andExpect(status().isOk())
				.andExpect(jsonPath("$.projectName", is("Project_1"))).andDo(print());

	}

	@Test
	public void updateEndProjectNotAvilable() throws Exception {

		when(projectService.getProject(project1.getProjectId())).thenReturn(null);

		mockMvc.perform(put("/endProject/{id}", project1.getProjectId()).contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.ObjecttoJSON(project1))).andExpect(status().isNotFound()).andDo(print());

	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
