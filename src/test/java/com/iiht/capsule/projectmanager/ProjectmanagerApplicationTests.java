package com.iiht.capsule.projectmanager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectmanagerApplication.class)
public class ProjectmanagerApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void main() {
		ProjectmanagerApplication.main(new String[] {});
	}

}
