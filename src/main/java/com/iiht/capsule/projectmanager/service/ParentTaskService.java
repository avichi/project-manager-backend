package com.iiht.capsule.projectmanager.service;

import java.util.List;

import com.iiht.capsule.projectmanager.model.ParentTask;

public interface ParentTaskService {

	ParentTask addParentTask(ParentTask parentTask);

	List<ParentTask> getAllParentTasks();

	ParentTask getParentTask(Long Id);

	ParentTask getParentTaskByName(String parentTaskName);

	ParentTask updateParentTask(ParentTask parentTask, Long Id);

	void deleteParentTask(Long Id);
}
