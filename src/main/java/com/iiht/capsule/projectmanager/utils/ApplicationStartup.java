package com.iiht.capsule.projectmanager.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.service.ParentTaskService;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationStartup.class);

	@Autowired
	ParentTaskService parentTaskService;

	/**
	 * This event is executed as late as conceivably possible to indicate that the application is ready to service
	 * requests.
	 */
	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		LOGGER.info("************************************Application Started*********************************");
		ParentTask pt = new ParentTask();
		pt.setParentTaskName("No Parent Task");
		parentTaskService.addParentTask(pt);
		return;
	}

}
