package com.iiht.capsule.projectmanager.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iiht.capsule.projectmanager.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
