package com.iiht.capsule.projectmanager.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.repo.ParentTaskRepository;

public class ParentTaskServiceTest {

	@InjectMocks
	ParentTaskServiceImpl parentTaskService;

	@Mock
	ParentTaskRepository parentTaskRepo;

	private ParentTask pt1 = new ParentTask();

	private ParentTask pt2 = new ParentTask();

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		pt1.setId(1);
		pt1.setParentTaskName("parent_task_1");
		pt2.setId(2);
		pt2.setParentTaskName("parent_task_2");
	}

	@Test
	public void getAllParentTaskTest() throws ParseException {

		List<ParentTask> parentTask = new ArrayList<ParentTask>();
		parentTask.add(pt1);
		parentTask.add(pt2);
		when(parentTaskRepo.findAll()).thenReturn(parentTask);
		List<ParentTask> parentTaskList = parentTaskService.getAllParentTasks();
		assertEquals(2, parentTaskList.size());
		verify(parentTaskRepo, times(1)).findAll();

	}

	@Test
	public void createParentTaskTest() throws ParseException {
		parentTaskService.addParentTask(pt1);
		verify(parentTaskRepo, times(1)).save(pt1);
	}

	@Test
	public void getParentTaskTest() {
		when(parentTaskRepo.findById(1L)).thenReturn(Optional.of(pt1));
		ParentTask pt = parentTaskService.getParentTask(1L);
		assertEquals(1, pt.getId());
		assertEquals("parent_task_1", pt.getParentTaskName());
	}

	@Test
	public void deletParentTaskTest() {
		parentTaskService.deleteParentTask(pt1.getId());
		verify(parentTaskRepo, times(1)).deleteById(pt1.getId());
		verifyNoMoreInteractions(parentTaskRepo);
	}

	@Test
	public void updateParentTaskTest() {

		when(parentTaskRepo.findById(pt2.getId())).thenReturn(Optional.of(pt1));
		parentTaskService.updateParentTask(pt2, pt2.getId());
		ArgumentCaptor<ParentTask> parentTaskArgument = ArgumentCaptor.forClass(ParentTask.class);
		verify(parentTaskRepo, times(1)).save(parentTaskArgument.capture());
		ParentTask parentTaskValue = parentTaskArgument.getValue();
		assertNotNull(parentTaskValue.getId());
		assertThat(parentTaskValue.getParentTaskName(), is(pt2.getParentTaskName()));

	}

	@Test
	public void getParentTaskNyNameTest() {
		when(parentTaskRepo.findByParentTaskName(pt1.getParentTaskName())).thenReturn(pt1);
		ParentTask pt = parentTaskService.getParentTaskByName(pt1.getParentTaskName());
		assertEquals(1, pt.getId());
		assertEquals("parent_task_1", pt.getParentTaskName());
	}

}
