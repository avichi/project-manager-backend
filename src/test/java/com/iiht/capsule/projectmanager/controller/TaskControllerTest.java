package com.iiht.capsule.projectmanager.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.model.Task;
import com.iiht.capsule.projectmanager.model.TestDataService;
import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.service.ProjectService;
import com.iiht.capsule.projectmanager.service.TaskService;
import com.iiht.capsule.projectmanager.service.UserService;
import com.iiht.capsule.projectmanager.util.TestUtil;
import com.jparams.verifier.tostring.ToStringVerifier;

@RunWith(SpringRunner.class)
@ContextConfiguration({ "classpath*:spring-test.xml" })
public class TaskControllerTest {

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Mock
	private TaskService taskService;

	@Mock
	private UserService userService;

	@Mock
	private ProjectService projectService;

	private MockMvc mockMvc;

	private Project project1 = new Project();

	private Project project2 = new Project();

	private User manager = new User();

	private User user2 = new User();

	private ParentTask pt = new ParentTask();

	private Task task1 = new Task();

	private Task task2 = new Task();

	private TestDataService testDataService = new TestDataService();

	@InjectMocks
	private TaskController taskController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(taskController).build();
		testDataService.getTestData(manager, user2, project1, project2, pt, task1, task2);
	}

	@Test
	public void getAllTasks() throws IOException, ParseException, Exception {

		List<Task> tasks = new ArrayList<Task>();
		tasks.add(task1);
		tasks.add(task2);

		when(taskService.getAllTasks()).thenReturn(tasks);
		mockMvc.perform(get("/tasks")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].taskId", is(1))).andExpect(jsonPath("$[0].startDate", is(notNullValue())))
				.andExpect(jsonPath("$[0].endDate", is(notNullValue()))).andExpect(jsonPath("$[0].priority", is(20)))
				.andExpect(jsonPath("$[0].isCompleted", is(false)))
				.andExpect(jsonPath("$[0].parentTask.parentTaskName", is("Parent_Task")))
				.andExpect(jsonPath("$[0].user.userId", is(1))).andExpect(jsonPath("$[0].project.projectId", is(1)))
				.andExpect(jsonPath("$[0].task", is("Task_1"))).andExpect(jsonPath("$[1].taskId", is(2)))
				.andExpect(jsonPath("$[1].startDate", is(notNullValue())))
				.andExpect(jsonPath("$[1].endDate", is(notNullValue()))).andExpect(jsonPath("$[1].priority", is(10)))
				.andExpect(jsonPath("$[1].isCompleted", is(true)))
				.andExpect(jsonPath("$[1].parentTask.parentTaskName", is("Parent_Task")))
				.andExpect(jsonPath("$[1].user.userId", is(1))).andExpect(jsonPath("$[1].project.projectId", is(1)))
				.andExpect(jsonPath("$[1].task", is("Task_2"))).andDo(print());
		verify(taskService, times(1)).getAllTasks();
		verifyNoMoreInteractions(taskService);
	}

	@Test
	public void createTask() throws Exception {

		when(taskService.addTask(task1)).thenReturn(task1);

		mockMvc.perform(post("/tasks").contentType(APPLICATION_JSON_UTF8).content(TestUtil.ObjecttoJSON(task1)))
				.andExpect(status().isCreated()).andExpect(jsonPath("$.taskId", is(1)))
				.andExpect(jsonPath("$.startDate", is(notNullValue())))
				.andExpect(jsonPath("$.endDate", is(notNullValue()))).andExpect(jsonPath("$.priority", is(20)))
				.andExpect(jsonPath("$.isCompleted", is(false)))
				.andExpect(jsonPath("$.parentTask.parentTaskName", is("Parent_Task")))
				.andExpect(jsonPath("$.user.userId", is(1))).andExpect(jsonPath("$.project.projectId", is(1)))
				.andExpect(jsonPath("$.task", is("Task_1"))).andDo(print());
	}

	@Test
	public void getTask() throws Exception {

		when(taskService.getTask(1L)).thenReturn(task1);

		mockMvc.perform(get("/tasks/{id}", "1")).andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void getTaskNotAvailable() throws Exception {
		when(taskService.getTask(1L)).thenReturn(null);

		mockMvc.perform(get("/tasks/{id}", "1")).andExpect(status().is4xxClientError()).andDo(print());
	}

	@Test
	public void deleteTask() throws Exception {

		when(taskService.getTask(task1.getTaskId())).thenReturn(task1);

		mockMvc.perform(delete("/tasks/{id}", task1.getTaskId())).andExpect(status().isOk()).andDo(print());

	}

	@Test
	public void deleteTaskNotAvailable() throws Exception {
		when(taskService.getTask(1L)).thenReturn(null);

		mockMvc.perform(delete("/tasks/{id}", "1")).andExpect(status().isNotFound()).andDo(print());

	}

	@Test
	public void updateTask() throws Exception {

		when(taskService.getTask(task2.getTaskId())).thenReturn(task1);
		when(taskService.updateTask(task2, task2.getTaskId())).thenReturn(task1);

		mockMvc.perform(put("/tasks/{id}", task2.getTaskId()).contentType(APPLICATION_JSON_UTF8)
				.content(TestUtil.ObjecttoJSON(task1))).andExpect(status().isOk())
				.andExpect(jsonPath("$.project.projectName", is("Project_1"))).andDo(print());

	}

	@Test
	public void updateTaskNotAvilable() throws Exception {

		when(taskService.getTask(task1.getTaskId())).thenReturn(null);

		mockMvc.perform(put("/tasks/{id}", task1.getTaskId()).contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.ObjecttoJSON(task1))).andExpect(status().isNotFound()).andDo(print());

	}

	@Test
	public void updateEndTask() throws Exception {

		when(taskService.getTask(task2.getTaskId())).thenReturn(task1);
		when(taskService.updateTask(task2, task2.getTaskId())).thenReturn(task1);

		mockMvc.perform(put("/endtask/{id}", task2.getTaskId()).contentType(APPLICATION_JSON_UTF8)
				.content(TestUtil.ObjecttoJSON(task1))).andExpect(status().isOk())
				.andExpect(jsonPath("$.project.projectName", is("Project_1"))).andDo(print());

	}

	@Test
	public void updateEndTaskNotAvilable() throws Exception {

		when(taskService.getTask(task1.getTaskId())).thenReturn(null);

		mockMvc.perform(put("/endtask/{id}", task1.getTaskId()).contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.ObjecttoJSON(task1))).andExpect(status().isNotFound()).andDo(print());

	}

	@Test
	public void myTest() {
		ToStringVerifier.forClass(ParentTask.class).verify();
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
