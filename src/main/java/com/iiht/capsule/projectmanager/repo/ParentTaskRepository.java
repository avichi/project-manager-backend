package com.iiht.capsule.projectmanager.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iiht.capsule.projectmanager.model.ParentTask;

@Repository
public interface ParentTaskRepository extends CrudRepository<ParentTask, Long> {

	ParentTask findByParentTaskName(String parentTaskName);

}
