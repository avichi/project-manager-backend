package com.iiht.capsule.projectmanager.service;

import java.util.List;

import com.iiht.capsule.projectmanager.model.Task;

public interface TaskService {

	Task addTask(Task task);

	List<Task> getAllTasks();

	Task getTask(Long Id);

	Task endUpdateTask(Task task, Long Id);

	Task updateTask(Task task, Long Id);

	void deleteTask(Long Id);
}
