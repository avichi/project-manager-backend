package com.iiht.capsule.projectmanager.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iiht.capsule.projectmanager.model.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {

}
