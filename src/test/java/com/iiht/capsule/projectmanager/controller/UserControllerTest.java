package com.iiht.capsule.projectmanager.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iiht.capsule.projectmanager.model.ParentTask;
import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.model.Task;
import com.iiht.capsule.projectmanager.model.TestDataService;
import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.service.UserService;
import com.iiht.capsule.projectmanager.util.TestUtil;

@RunWith(SpringRunner.class)
@ContextConfiguration({ "classpath*:spring-test.xml" })
public class UserControllerTest {

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Mock
	private UserService userService;

	private MockMvc mockMvc;

	private Project project1 = new Project();

	private Project project2 = new Project();

	private User user1 = new User();

	private User user2 = new User();

	private ParentTask pt = new ParentTask();

	private Task task1 = new Task();

	private Task task2 = new Task();

	private TestDataService testDataService = new TestDataService();

	@InjectMocks
	private UserController userController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
		testDataService.getTestData(user1, user2, project1, project2, pt, task1, task2);
	}

	@Test
	public void getAllUsers() throws IOException, ParseException, Exception {

		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);

		when(userService.getAllUsers()).thenReturn(users);
		mockMvc.perform(get("/users")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].userId", is(1))).andExpect(jsonPath("$[0].firstName", is("FN")))
				.andExpect(jsonPath("$[0].lastName", is("LN"))).andExpect(jsonPath("$[0].employeeId", is("1")))
				.andExpect(jsonPath("$[1].userId", is(2))).andExpect(jsonPath("$[1].firstName", is("FName")))
				.andExpect(jsonPath("$[1].lastName", is("LName"))).andExpect(jsonPath("$[1].employeeId", is("2")))
				.andDo(print());
		verify(userService, times(1)).getAllUsers();
		verifyNoMoreInteractions(userService);
	}

	@Test
	public void createUser() throws Exception {

		when(userService.addUser(user1)).thenReturn(user1);

		mockMvc.perform(post("/users").contentType(APPLICATION_JSON_UTF8).content(TestUtil.ObjecttoJSON(user1)))
				.andExpect(status().isCreated()).andExpect(jsonPath("$.userId", is(1)))
				.andExpect(jsonPath("$.firstName", is("FN"))).andExpect(jsonPath("$.lastName", is("LN")))
				.andExpect(jsonPath("$.employeeId", is("1"))).andDo(print());
	}

	@Test
	public void getUser() throws Exception {

		when(userService.getUser(1L)).thenReturn(user1);

		mockMvc.perform(get("/users/{id}", "1")).andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void getUserNotAvailable() throws Exception {
		when(userService.getUser(1L)).thenReturn(null);

		mockMvc.perform(get("/users/{id}", "1")).andExpect(status().is4xxClientError()).andDo(print());
	}

	@Test
	public void deleteUser() throws Exception {

		when(userService.getUser(1L)).thenReturn(user1);

		mockMvc.perform(delete("/users/{id}", user1.getUserId())).andExpect(status().isOk()).andDo(print());

	}

	@Test
	public void updateUser() throws Exception {
		when(userService.getUser(user2.getUserId())).thenReturn(user1);
		when(userService.updateUser(user2, user2.getUserId())).thenReturn(user1);

		mockMvc.perform(put("/users/{id}", user2.getUserId()).contentType(APPLICATION_JSON_UTF8)
				.content(TestUtil.ObjecttoJSON(user1))).andExpect(status().isOk())
				.andExpect(jsonPath("$.employeeId", is("1"))).andDo(print());

	}

	@Test
	public void updateUserNotAvilable() throws Exception {

		when(userService.getUser(user1.getUserId())).thenReturn(null);

		mockMvc.perform(put("/users/{id}", user1.getUserId()).contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.ObjecttoJSON(user1))).andExpect(status().isNotFound()).andDo(print());

	}

	@Test
	public void deleteUserNotAvailable() throws Exception {
		when(userService.getUser(1L)).thenReturn(null);

		mockMvc.perform(delete("/users/{id}", "1")).andExpect(status().isNotFound()).andDo(print());

	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
