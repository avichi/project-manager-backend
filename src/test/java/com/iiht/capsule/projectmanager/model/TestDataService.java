package com.iiht.capsule.projectmanager.model;

import java.time.LocalDate;

public class TestDataService {
	public void getTestData(User user1, User user2, Project project1, Project project2, ParentTask pt, Task task1,
			Task task2) {

		System.out.print("******************************Test Started******************************");
		LocalDate startdate1 = LocalDate.of(2017, 1, 13);
		LocalDate startdate2 = LocalDate.of(2017, 2, 13);
		LocalDate enddate1 = LocalDate.of(2017, 3, 13);
		LocalDate enddate2 = LocalDate.of(2017, 4, 13);
		user1.setEmployeeId("1");
		user1.setFirstName("FN");
		user1.setLastName("LN");
		user1.setUserId(1);
		user2.setEmployeeId("2");
		user2.setFirstName("FName");
		user2.setLastName("LName");
		user2.setUserId(2);
		project1.setCompletedTasks(10);
		project1.setEndDate(enddate1);
		project1.setIsCompleted(false);
		project1.setManager(user1);
		project1.setNumberofTasks(20);
		project1.setPriority(10);
		project1.setProjectId(1);
		project1.setProjectName("Project_1");
		project1.setStartDate(startdate1);
		project2.setCompletedTasks(10);
		project2.setEndDate(enddate2);
		project2.setIsCompleted(true);
		project2.setManager(user1);
		project2.setNumberofTasks(20);
		project2.setPriority(20);
		project2.setProjectId(2);
		project2.setProjectName("Project_2");
		project2.setStartDate(startdate2);
		pt.setId(1);
		pt.setParentTaskName("Parent_Task");
		task1.setStartDate(startdate1);
		task1.setEndDate(enddate1);
		task1.setParentTask(pt);
		task1.setIsCompleted(false);
		task1.setPriority(20);
		task1.setTask("Task_1");
		task1.setTaskId(1);
		task1.setProject(project1);
		task1.setUser(user1);
		task2.setStartDate(startdate2);
		task2.setEndDate(enddate2);
		task2.setParentTask(pt);
		task2.setIsCompleted(true);
		task2.setPriority(10);
		task2.setTask("Task_2");
		task2.setTaskId(2);
		task2.setProject(project1);
		task2.setUser(user1);

	}

}
