package com.iiht.capsule.projectmanager.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iiht.capsule.projectmanager.model.Project;
import com.iiht.capsule.projectmanager.service.ProjectService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ProjectController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

	@Autowired
	private ProjectService projectService;

	@RequestMapping("/projects")
	public List<Project> getAllProjects() {
		return projectService.getAllProjects();
	}

	@RequestMapping("/projects/{id}")
	public ResponseEntity<Object> getProject(@PathVariable long id) {
		LOGGER.info("**********ID***********" + id);
		Project updatedProject = projectService.getProject(id);

		if (updatedProject != null) {
			return ResponseEntity.ok(updatedProject);
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found with ID " + id);

	}

	@PostMapping("/projects")
	public ResponseEntity<Object> createProject(@RequestBody Project project) {
		LOGGER.info("********************" + project.toString());
		projectService.addProject(project);
		return new ResponseEntity<Object>(project, HttpStatus.CREATED);

	}

	@PutMapping("/endProject/{id}")
	public ResponseEntity<Object> endProject(@RequestBody Project project, @PathVariable Long id) {
		LOGGER.info("*********Project***********" + project.toString());
		LOGGER.info("**********ID***********" + id);
		Project Project = projectService.getProject(id);
		if (Project != null) {
			projectService.endUpdateProject(project, id);
			return new ResponseEntity<Object>(project, HttpStatus.OK);
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Task not found with ID " + id);

	}

	@PutMapping("/projects/{id}")
	public ResponseEntity<Object> updateProject(@RequestBody Project project, @PathVariable Long id) {
		LOGGER.info("*********Project***********" + project.toString());
		LOGGER.info("**********ID***********" + id);

		Project Project = projectService.getProject(id);
		if (Project != null) {
			projectService.updateProject(project, id);
			return new ResponseEntity<Object>(project, HttpStatus.OK);
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found with ID " + id);

	}

	@DeleteMapping("/projects/{id}")
	public ResponseEntity<Object> deleteProject(@PathVariable Long id) {
		LOGGER.info("**********ID***********" + id);
		Project Project = projectService.getProject(id);

		if (Project != null) {
			projectService.deleteProject(id);

			return ResponseEntity.status(HttpStatus.OK).body("");
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found with ID " + id);

	}

}
