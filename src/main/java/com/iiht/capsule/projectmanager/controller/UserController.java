package com.iiht.capsule.projectmanager.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iiht.capsule.projectmanager.model.User;
import com.iiht.capsule.projectmanager.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@RequestMapping("/users/{id}")
	public ResponseEntity<Object> getUser(@PathVariable long id) {
		LOGGER.info("**********ID***********" + id);
		User getUser = userService.getUser(id);

		if (getUser != null) {
			return ResponseEntity.ok(getUser);
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found with ID " + id);

	}

	@PostMapping("/users")
	public ResponseEntity<Object> createUser(@RequestBody User user) {
		LOGGER.info("********************" + user.toString());
		userService.addUser(user);
		return new ResponseEntity<Object>(user, HttpStatus.CREATED);

	}

	@PutMapping("/users/{id}")
	public ResponseEntity<Object> updateUser(@RequestBody User user, @PathVariable Long id) {
		LOGGER.info("*********User***********" + user.toString());
		LOGGER.info("**********ID***********" + id);
		User User = userService.getUser(id);

		if (User != null) {
			userService.updateUser(user, id);
			return new ResponseEntity<Object>(user, HttpStatus.OK);
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found with ID " + id);

	}

	@DeleteMapping("/users/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable Long id) {
		LOGGER.info("**********ID***********" + id);
		User User = userService.getUser(id);

		if (User != null) {
			userService.deleteUser(id);

			return ResponseEntity.status(HttpStatus.OK).body("");
		}
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found with ID " + id);

	}

}
